package com.jpa.master.jpademo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import com.jpa.master.jpademo.entities.Course;
import com.jpa.master.jpademo.entities.Review;
import com.jpa.master.jpademo.repositories.CourseRepository;

@SpringBootTest
class CourseRepositoryTests {

	@Autowired
	private CourseRepository sut;


	@Test
	void should_return_all_recrds() {
		// Arrange
		// Act
		var results = sut.getAll();
		// Assert
		assertNotNull(results);
		assertTrue(results.size() > 0);
	}

	@Test
	void findById_should_return_matching_record() {
		// Arrange
		var courseId = 10001L;
		// Act
		var results = sut.findById(courseId);
		// Assert
		assertEquals("JPA Demo Course", results.getName());
	}

	@Test
	@DirtiesContext
	void deleteById_should_remove_record_matching_the_given_id() {
		// Arrange
		var courseId = 10001L;
		// Act
		sut.deleteById(courseId);
		var results = sut.findById(courseId);
		// Assert
		assertEquals(null, results);
	}

	@Test
	@DirtiesContext
	void save_should_persit_a_record_to_the_database() {
		// Arrange
		var course = new Course(null, "Algorithims and data structures", LocalDateTime.now(), LocalDateTime.now(),
				getReviews(), null);
		// Act
		sut.save(course);
		var results = sut.findById(course.getId());
		// Assert
		assertEquals(course.getName(), results.getName());
	}

	@Test
	@DirtiesContext
	void save_should_update_a_matching_record_to_the_database() {
		// Arrange
		var course = new Course(10002L, "Java unit testing", LocalDateTime.now(), LocalDateTime.now(), getReviews(),
				null);
		// Act
		sut.save(course);
		var results = sut.findById(course.getId());
		// Assert
		assertEquals(course.getName(), results.getName());
	}

	@Test
	void play_with_entity_manager() {
		// Arrange
		var courseName = "Java unit testing";
		var course = new Course(null, courseName, LocalDateTime.now(), LocalDateTime.now(), getReviews(), null);
		// Act
		sut.playWithEntityManager(course);
		var results = sut.findById(course.getId());
		// Assert
		assertNotEquals(courseName, results.getName());
	}

	private List<Review> getReviews() {
		List<Review> reviews = new ArrayList<>();
		var course = new Course();
		course.setId(10001L);
		reviews.add(new Review(40001L, "Great Course", "5", course));
		reviews.add(new Review(40001L, "Awesome Course", "5", course));
		reviews.add(new Review(40001L, "Fantastic Course", "5", course));

		return reviews;
	}
}
