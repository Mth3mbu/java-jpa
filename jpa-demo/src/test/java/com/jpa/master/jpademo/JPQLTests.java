package com.jpa.master.jpademo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.jpa.master.jpademo.entities.Course;

import jakarta.persistence.EntityManager;

@SpringBootTest
class JPQLTests {

    @Autowired
    EntityManager entityManager;

    @Test
    public void JPQL_should_return_all_records() {
        // Arrange
        var query = entityManager.createQuery("SELECT c FROM Course c", Course.class);
        // Act
        List<Course> courses = query.getResultList();
        // Assert
        assertNotNull(courses);
        assertTrue(courses.size() > 0);
    }

    @Test
    public void native_query_should_return_all_records() {
        // Arrange
        var query = entityManager.createNativeQuery("SELECT * FROM COURSE", Course.class);
        // Act
        List<Course> courses = query.getResultList();
        // Assert
        assertNotNull(courses);
        assertTrue(courses.size() > 0);
    }

    @Test
    public void shoud_type_query_database_by_Condition() {
        // Arrange
        var courseName = "JPA Demo Course";
        var query = entityManager.createQuery("SELECT c FROM Course c WHERE name like '%JPA%'", Course.class);
        // Act
        List<Course> courses = query.getResultList();
        // Assert
        assertNotNull(courses);
        assertTrue(courses.size() > 0);
        assertEquals(courses.get(0).getName(), courseName);
    }
}