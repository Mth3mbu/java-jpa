package com.jpa.master.jpademo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import com.jpa.master.jpademo.entities.Passport;
import com.jpa.master.jpademo.entities.Student;
import com.jpa.master.jpademo.repositories.StudentRepository;

@SpringBootTest
public class StudentRepositoryTests {
    @Autowired
    private StudentRepository sut;

    @Test
    public void should_get_all_students() {
        // Arrange
        // Act
        var results = sut.getAll();
        // Assert
        assertNotNull(results);
        assertTrue(results.size() > 1);
    }

    @Test
    public void should_get_student_matching_given_id() {
        // Arrange
        var studentId = 20001L;
        var studentName = "Lyle";
        // Act
        var results = sut.findById(studentId);
        // Assert
        assertNotNull(results);
        assertEquals(studentName, results.getName());
    }

    @Test
    @DirtiesContext
    public void should_create_new_student() {
        // Arrange
        var passPort = new Passport(null, "ND3004");
        var student = new Student(null, "Mags", passPort, null);
        // Act
        var results = sut.saveOrUpdate(student);
        // Assert
        assertNotNull(results);
        assertEquals(student.getName(), results.getName());
        assertEquals(student.getId(), results.getId());
    }

    @Test
    @DirtiesContext
    public void should_udpate_student() {
        // Arrange
        var passPort = new Passport(30001L, "ND3004");
        var student = new Student(20001L, "Spike", passPort, null);
        // Act
        var results = sut.saveOrUpdate(student);
        var updateStudent = sut.findById(student.getId());
        // Assert
        assertNotNull(results);
        assertEquals(student.getName(), updateStudent.getName());
    }
}
