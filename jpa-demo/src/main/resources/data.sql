insert into course (id, name, created_date,last_updated_date) values (10001,'JPA Demo Course',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())
insert into course (id, name, created_date,last_updated_date) values (10002,'SQL Bootcamp',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())
insert into course (id, name, created_date,last_updated_date) values (10003,'C# Master Class',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())
insert into course (id, name, created_date,last_updated_date) values (10004,'Java',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())

insert into Passport(id, number) values (30001,'NUR8804')
insert into Passport(id, number) values (30002, 'NES8805')
insert into Passport(id, number) values (30003,'NUB8806')
insert into Passport(id, number) values (30004,'NKR8807')

insert into Student(id, name, passport_id) values (20001,'Lyle', 30001)
insert into Student(id, name, passport_id) values (20002, 'Arnold',30002)
insert into Student(id, name, passport_id) values (20003,'Ben',30003)
insert into Student(id, name, passport_id) values (20004,'John',30004)

insert into Review(id, description, rating,course_id) values (40001,'Great Course', '5',10001)
insert into Review(id, description, rating,course_id) values (40002, 'Top content', '4',10001)
insert into Review(id, description, rating,course_id) values (40003,'Good grammar','3',10002)
insert into Review(id, description, rating,course_id) values (40004,'Long videos','1',10002)


insert into student_course(student_id,course_id) values(20001,10001)
insert into student_course(student_id,course_id) values(20002,10001)
insert into student_course(student_id,course_id) values(20003,10001)
insert into student_course(student_id,course_id) values(20004,10002)