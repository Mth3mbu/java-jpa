package com.jpa.master.jpademo.repositories;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.jpa.master.jpademo.entities.Student;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@Repository
@Transactional
public class StudentRepository {

    private EntityManager entityManager;

    public StudentRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Student> getAll() {
        var query = entityManager.createQuery("SELECT s FROM Student s", Student.class);
        return query.getResultList();
    }

    public Student findById(Long id) {
        return entityManager.find(Student.class, id);
    }

    public void delete(Long id) {
        var student = entityManager.find(Student.class, id);
        if (student != null)
            entityManager.remove(student);
    }

    public Student saveOrUpdate(Student student) {
        if (student.getId() == null) {
            entityManager.persist(student.getPassport());
            entityManager.persist(student);
        } else {
            entityManager.merge(student);
        }
        return student;
    }
}
