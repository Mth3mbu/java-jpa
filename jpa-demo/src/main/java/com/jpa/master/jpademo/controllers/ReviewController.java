package com.jpa.master.jpademo.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jpa.master.jpademo.models.AddReviewRequest;
import com.jpa.master.jpademo.repositories.ReviewRepository;

@RestController
public class ReviewController {
    private final ReviewRepository reviewRepository;

    public ReviewController(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @PostMapping("review")
    public void addReview(@RequestBody AddReviewRequest request) {
        reviewRepository.addReview(request);
    }
}
