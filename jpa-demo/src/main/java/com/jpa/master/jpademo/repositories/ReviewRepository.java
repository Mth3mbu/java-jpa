package com.jpa.master.jpademo.repositories;

import org.springframework.stereotype.Repository;

import com.jpa.master.jpademo.entities.Course;
import com.jpa.master.jpademo.models.AddReviewRequest;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@org.springframework.transaction.annotation.Transactional

@Repository
@Transactional
public class ReviewRepository {

    private EntityManager entityManager;

    public ReviewRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void addReview(AddReviewRequest request) {
        var course = entityManager.find(Course.class, request.getCourseId());
        var review = request.getReview();
        review.setCourse(course);
        entityManager.persist(review);
    }
}
