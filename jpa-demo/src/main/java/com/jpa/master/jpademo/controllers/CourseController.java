package com.jpa.master.jpademo.controllers;

import com.jpa.master.jpademo.entities.Course;
import com.jpa.master.jpademo.repositories.CourseRepository;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseController {

    private final CourseRepository courseRepository;

    public CourseController(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @GetMapping("course")
    public List<Course> createCourse(){
         return courseRepository.getAll();
    }

    @PostMapping("course")
    public Course createCourse(@RequestBody Course course) {
        return courseRepository.save(course);
    }

    @PutMapping("course")
    public Course updateCourse(@RequestBody Course course) {
        return courseRepository.save(course);
    }

    @GetMapping("course/{id}")
    public Course getCourseById(@PathVariable("id") Long id) {
        return courseRepository.findById(id);
    }

    @DeleteMapping("course/{id}")
    public void deleteCourseById(@PathVariable("id") Long id) {
        courseRepository.deleteById(id);
    }
}
