package com.jpa.master.jpademo.repositories;

import com.jpa.master.jpademo.entities.Course;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class CourseRepository {

    private final EntityManager entityManager;

    public CourseRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Course> getAll() {
        var query = entityManager.createNamedQuery("get_all_courses", Course.class);
       var res= query.getResultList();

       return res;
    }

    public Course findById(Long id) {
        return entityManager.find(Course.class, id);
    }

    public void deleteById(Long id) {
        var course = findById(id);
        entityManager.remove(course);
    }

    public Course save(Course course) {
        if (course.getId() == null) {
            entityManager.persist(course);
        } else
            entityManager.merge(course);
        return course;
    }

    public void playWithEntityManager(Course course) {
        entityManager.persist(course);
        course.setName("BigO notation");
    }

}
