package com.jpa.master.jpademo.models;

import com.jpa.master.jpademo.entities.Review;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class AddReviewRequest {
    private Long courseId;
    private Review review;
}
