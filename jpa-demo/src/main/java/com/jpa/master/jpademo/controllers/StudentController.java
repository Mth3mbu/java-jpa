package com.jpa.master.jpademo.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jpa.master.jpademo.entities.Student;
import com.jpa.master.jpademo.repositories.StudentRepository;

import jakarta.transaction.Transactional;

@RestController
public class StudentController {
    private final StudentRepository studentRepository;

    public StudentController(StudentRepository repository) {
        this.studentRepository = repository;
    }

    @PostMapping("student")
    public Student save(@RequestBody Student student) {
        return studentRepository.saveOrUpdate(student);
    }

    @PutMapping("student")
    public Student update(@RequestBody Student student) {
        return studentRepository.saveOrUpdate(student);
    }

    @GetMapping("student/all")
    @Transactional
    public List<Student> getAll() {
        return studentRepository.getAll();
    }

    @GetMapping("student/{id}")
    public Student getById(@PathVariable("id") Long id) {
        return studentRepository.findById(id);
    }

    @DeleteMapping("student/{id}")
    public void delete(@PathVariable("id") Long id) {
        studentRepository.delete(id);
    }
}
